# Openpayments-ember

This README outlines the details of collaborating on this Ember application.
A short introduction of this app could easily go here.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://www.ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember server`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://www.ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

## Directory Structure

* app: This is where folders and files for models, components, routes, templates and styles are stored. The majority of your coding on an Ember project happens in this folder.

* bower_components / bower.json: Bower is a dependency management tool. It is used in Ember CLI to manage front-end plugins and component dependencies (HTML, CSS, JavaScript, etc). All Bower components are installed in the bower_components directory. If we open bower.json, we see the list of dependencies that are installed automatically including Ember, jQuery, Ember Data and QUnit (for testing). If we add additional front-end dependencies, such as Bootstrap, we will see them listed here and added to the bower_components directory.

* config: The config directory contains the environment.js where you can configure settings for your app.

* dist: When we build our app for deployment, the output files will be created here.

* node_modules / package.json: This directory and file are from npm. npm is the package manager for Node.js. Ember is built with Node and uses a variety of Node.js modules for operation. The package.json file maintains the list of current npm dependencies for the app. Any Ember-CLI add-ons you install will also show up here. Packages listed in package.json are installed in the node_modules directory.

* public: This directory contains assets such as images and fonts.

* vendor: This directory is where front-end dependencies (such as JavaScript or CSS) that are not managed by Bower go.

* tests / testem.json: Automated tests for our app go in the tests folder, and Ember CLI's test runner testem is configured in testem.json.

* tmp: Ember CLI temporary files live here.

* ember-cli-build.js: This file describes how Ember CLI should build our app.
